package nl.utwente.di.bookQuote;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestFahrenheitCalc {

    @Test
    public void testBook1() throws Exception {
        FahrenheitCalc quoter = new FahrenheitCalc();
        double price = quoter.getBookPrice(1.0);
        Assertions.assertEquals(33.8, price, 0.0,"Priceofbook1");
    }
}
