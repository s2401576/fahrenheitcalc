package nl.utwente.di.bookQuote;

public class FahrenheitCalc {
    public Double getBookPrice(Double celcius) {
        return ((celcius*1.8)+32);
    }
}
